# Restaurant

Student project of restaurant service.


## Database
MongoDB instance running in Docker container.   
Port:27017

First run:  
```
sudo docker-compose up
```
Start:
```
sudo docker-compose start
```
Stop:
```
sudo docker-compose stop
```
Remove:
```
sudo docker-compose down
```

IMPORTANT only for development purposes!  
BEFORE deploy on production configure login and password. 

###Initial database
You can init database with additional data by adding scripts to init-mongo.js.
More information in MongoDB [documentation](https://docs.mongodb.com/).

##SECURITY

###Getting JWT

```$xslt
POST:http://localhost:8080/login/
```
POST body
```json
{
  "username": "user",
  "password": "password"
}
```
###HEADER
```
Authorization:{TOKEN}
```
###USERS
```
boss:boss
waiter:waiter
cook:cook
```

###Rest securing example
In package ``org.studies.restaurant.security.controller`` class ``ExampleController``

