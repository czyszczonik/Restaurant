import {useState, useEffect} from "react";
import FetchAllOrders from "../../services/waiter/order/FetchAllOrders";
import FetchActiveOrders from "../../services/waiter/order/FetchActiveOrders";
import RenderOrders from "./RenderOrders"

export default function OrdersTab(props, handleUpdate){

    const [data, setData] = useState(false)

    useEffect(()=> {
      getContent();
    },[props]) // LOL DON't DELETE this  =>  ,[props]

    async function getContent(){
      let response = await FetchAllOrders(props);
      response = RenderOrders(response, props, handleUpdate)
      setData(response)
    }

    return(data);
}
