import React, {useState} from "react";
import { FormGroup, FormControl, ControlLabel, Button } from "react-bootstrap";
import "./AddOrder.css";
import CreateNewOrder from "../../../services/waiter/order/CreateNewOrder";

export default function AddOrder(props, handleUpdate) {

  const [tableNumber, setTableNumber] = useState("");

  function handleSubmit(event){
    event.preventDefault();
    let value = CreateNewOrder(props, tableNumber)
    props.history.push("/orders")
  }


  function validateForm(){
    return tableNumber.length > 0;
  }

  return (
    <div className="AddOrder">
    <h4>Add Order</h4>
      <form onSubmit={handleSubmit}>
        <FormGroup controlId="formBasicName">
          <ControlLabel>Table Number</ControlLabel>
          <FormControl type="text"
           placeholder="Table Number"
           value={tableNumber}
           onChange={e => setTableNumber(e.target.value)}
           />
        </FormGroup>

        <Button variant="primary"
          type="submit"
          disabled={!validateForm()}
          >
          Add
        </Button>
      </form>
    </div>
  );
}
