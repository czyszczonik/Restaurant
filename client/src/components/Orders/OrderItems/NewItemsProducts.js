import React from 'react';
import {useState} from "react";
import { FormGroup, FormControl, ControlLabel, Button } from "react-bootstrap";
import InsertNewOrderItem from "../../../services/waiter/order/InsertOrderItem"

export default function NewItemsProducts(props, order) {

    const [quantity, setQuantity] = useState("")
    const [currentProduct, setCurrentProduct] = useState("")
    const [currentOrder, setCurrentOrder] = useState(order)

    function handleSubmit(event) {
        event.preventDefault()
        InsertNewOrderItem(props, currentOrder.id, currentProduct, quantity)
    }

    function handleDone() {
        setQuantity("")
        setCurrentProduct("")
        props.history.push({
            pathname: "/orderItems",
            state: { order: order}
          })
    
    }

    function validateForm(){
        return quantity > 0 && currentProduct !== "";
    }

    return (
        <div>
            <h4>Add new Item</h4>
            <form onSubmit={handleSubmit}>
                <FormGroup>
                    <ControlLabel>Name</ControlLabel>
                    <FormControl type="text" placeholder="Product Name"
                        value={currentProduct} 
                        onChange={e => setCurrentProduct(e.target.value)}>

                    </FormControl>
                </FormGroup>
                <FormGroup controlId="formBasicQuantity">
                    <ControlLabel>Quantity</ControlLabel>
                    <FormControl type="number"
                        placeholder="quantity"
                        value={quantity}
                        onChange={e => setQuantity(e.target.value)}
                    />
                </FormGroup>
                <Button variant="primary" type="submit" disabled={!validateForm()}>
                    Add
                </Button>
            <Button variant="primary" onClick={handleDone}>
                Done
            </Button>
            </form>
        </div> 
    );
}
