import React from 'react'   
import {Table} from "react-bootstrap";
import OrderItemDelete from "../../../services/waiter/order/OrderItemDelete"

export default function OrderNewItemsTab(props, order) {

    function deleteItem(item, orderOld) {
        OrderItemDelete(props, item.id)
        props.history.push({
            pathname: "/orderItems",
            state: { order: orderOld.id }
        })
    }

    function handleDone() {
        props.history.push("/orders")
    }

    return(
        <div className="OrderNewItemsTab">
            <h4>Order Items</h4>
            <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>Item</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                    {
                      order.orderItems.length > 0 
                        ? (order.orderItems.map(item => (
                          <tr key={item.id}>
                            <td>{item.product.name}</td>
                            <td>{item.quantity}</td>
                            <td>{item.price}</td>
                            <td>
                              <button className="button muted-button" onClick={()=>deleteItem(item, order)}>Delete Item</button>
                            </td>
                          </tr>
                          ))) 
                        : (
                          <tr>
                            <td colSpan={4}>No Items</td>
                          </tr>
                          )
                    }
                    
                </tbody>
            </Table>
            <div className="pb-3">
               <button className="pull-right button muted-button" onClick={()=>handleDone()}>Done</button>
            </div>
        </div>
    );
}
