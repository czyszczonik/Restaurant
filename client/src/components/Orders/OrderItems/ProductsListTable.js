import React from 'react'   
import {Table} from "react-bootstrap";


export default function ProductsListTable(products) {
    return (
        <div className="AddItem">
            <h4>Order Items</h4>
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>Product</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        products.length > 0
                        ? (
                            products.map(product =>
                                <tr key={product.id}>
                                    <td>{product.name}</td>
                                    <td>{product.price}</td>
                                </tr>    
                                
                            )
                            
                        ) : (
                            <tr>
                                <td colSpan="2">No Products</td>
                            </tr>
                        )
                    }
                </tbody>
            </Table>
        </div>
    )
}