import {useState, useEffect} from "react";
import OrderNewItemsTab from "./OrderNewItemsTab"
import FetchOrder from '../../../services/waiter/order/FetchOrder'

export default function OrderItemsProducts(props, order, items){

    const [data, setData] = useState(false)

    useEffect(()=> {
      getContent();
    },[props]) // LOL DON't DELETE this  =>  ,[props]

    async function getContent(){
      let orderFetched = await FetchOrder(props, order.id)
      let response = OrderNewItemsTab(props, orderFetched)
      setData(response)
    }

    return(data);
}