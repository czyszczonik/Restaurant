import {useState, useEffect} from "react";
import FetchActiveProducts from "../../../services/waiter/product/FetchActiveProducts";
import ProductsListTable from "./ProductsListTable";


export default function ProductsList(props){

    const [data, setData] = useState(false)

    useEffect(()=> {
      getContent();
    },[props]) // LOL DON't DELETE this  =>  ,[props]

    async function getContent(){
      let response = await FetchActiveProducts(props);
      response = ProductsListTable(response)
      setData(response)
    }

    return(data);
}
