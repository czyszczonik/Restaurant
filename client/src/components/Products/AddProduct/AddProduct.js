import React, {useState} from "react";
import { FormGroup, FormControl, ControlLabel, Button } from "react-bootstrap";
import "./AddProduct.css";
import InsertProduct from "../../../services/boss/product/InsertProduct";

export default function AddProduct(props) {

  const [productName, setProductName] = useState("");
  const [price, setPrice] = useState("");

  function handleSubmit(event){
    event.preventDefault();
    InsertProduct(productName,price,props.token);
    props.history.push("/products")
  }

  function validateForm(){
    return productName.length > 0 && price >= 0;
  }

  return (
    <div className="AddProduct">
    <h4>Add product</h4>
      <form onSubmit={handleSubmit}>
        <FormGroup controlId="formBasicName">
          <ControlLabel>Product Name</ControlLabel>
          <FormControl type="text"
           placeholder="Product Name"
           value={productName}
           onChange={e => setProductName(e.target.value)}
           />
        </FormGroup>

        <FormGroup controlId="formBasicPrice">
          <ControlLabel>Price</ControlLabel>
          <FormControl type="text"
           pattern="[0-9]*"
           placeholder="Price"
           value={price}
           onChange={e => setPrice(e.target.value)}
           />
        </FormGroup>
        <Button variant="primary"
          type="submit"
          disabled={!validateForm()}
          >
          Add
        </Button>
      </form>
    </div>
  );
}
