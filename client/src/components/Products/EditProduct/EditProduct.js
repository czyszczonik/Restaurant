import React, {useState} from "react";
import { FormGroup, FormControl, ControlLabel, Button } from "react-bootstrap";
import "./EditProduct.css";
import UpdateProduct from "../../../services/boss/product/UpdateProduct";

export default function EditProduct(props, product, setEditing) {

  const [productName, setProductName] = useState(product.name);
  const [price, setPrice] = useState(product.price);

  function handleSubmit(event){
    event.preventDefault();
    UpdateProduct(product.id, productName, price, product.active ,props.token);
    setEditing(false);
    props.history.push("/products")
  }

  function handleCancel(){
    setEditing(false);
    props.history.push("/products")
  }

  function validateForm(){
    return productName.length > 0 && price >= 0;
  }

  return (
    <div className="EditProduct">
    <h4>Update product</h4>
      <form onSubmit={handleSubmit}>
        <FormGroup controlId="formBasicName">
          <ControlLabel>Product Name</ControlLabel>
          <FormControl type="text"
           defaultValue={product.name}
           onChange={e => setProductName(e.target.value)}
           />
        </FormGroup>

        <FormGroup controlId="formBasicPrice">
          <ControlLabel>Price</ControlLabel>
          <FormControl type="text"
           pattern="[0-9]*"
           defaultValue={product.price}
           onChange={e => setPrice(e.target.value)}
           />
        </FormGroup>
        <Button variant="primary"
          type="submit"
          disabled={!validateForm()}
          >
          Update
        </Button>
        <Button variant="primary"
          onClick={()=>handleCancel()}
          >
          Cancel
        </Button>
      </form>
    </div>
  );
}
