import {useState, useEffect} from "react";
import "./ProductsTable.css";
import FetchProducts from "../../../services/boss/product/FetchProducts";
import RenderProducts from "./RenderProducts"
export default function ProductsTable(props, handleUpdate){

const [data, setData] = useState(false)

useEffect(()=> {
  getContent();
},[props]) // LOL DON't DELETE this  =>  ,[props]

async function getContent(){
  let response = await FetchProducts(props);
  response = RenderProducts(response, props, handleUpdate)
  setData(response)
}

    return(data);
}
