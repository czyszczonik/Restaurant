import React from 'react'
import {Table} from "react-bootstrap";
import DeleteProduct from "../../../services/boss/product/DeleteProduct"
import ChangeActivityProduct from "../../../services/boss/product/ChangeActivityProduct"

export default function RenderProducts(products, props, handleUpdate){

  function handleDelete(id){
    DeleteProduct(id, props.token);
    props.history.push("/products");
  }

  function handleChangeActivity(product){
    ChangeActivityProduct(product.id, props.token);
    props.history.push("/products");
  }

  return(

    <div className="ProductsTable">
    <h4>ProductsTable</h4>
    <Table striped bordered hover>
        <thead>
          <tr>
            <th>Name</th>
            <th>Price</th>
            <th>Active</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
        {
          products.length > 0 ? (
          products.map(product => (
            <tr key={product.id}>
              <td>{product.name}</td>
              <td>{product.price}</td>
              <td>{product.active ?
                  <div class="check">&#x2714;</div>
                  :
                  <div class="cross">&#215;</div>
              }</td>

              <td>
                {product.active ?
                  <button className="button muted-button" onClick={()=>handleChangeActivity(product)}>Dissable</button>
                  :
                  <button className="button muted-button" onClick={()=>handleChangeActivity(product)}>Enable</button>
              }
                <button className="button muted-button" onClick={()=>handleUpdate(product)}>Update</button>
                <button className="button muted-button" onClick={()=>handleDelete(product.id)}>Delete</button>
              </td>
            </tr>
          ))
        ) : (
          <tr>
            <td colSpan={4}>No Products</td>
          </tr>
        )}
      </tbody>
    </Table>
    </div>
  );
}
