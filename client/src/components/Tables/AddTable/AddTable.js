import React, {useState} from "react";
import { FormGroup, FormControl, ControlLabel, Button } from "react-bootstrap";
import "./AddTable.css";
import InsertTable from "../../../services/boss/table/InsertTable";

export default function AddTable(props) {

  const [tableNumber, setTableNumber] = useState("");
  const [places, setPlaces] = useState("");

  function handleSubmit(event){
    event.preventDefault();
    InsertTable(tableNumber,places,props.token);
    props.history.push("/tables")
  }

  function validateForm(){
    return tableNumber.length > 0 && places >= 0;
  }

  return (
    <div className="AddTable">
    <h4>Add Table</h4>
      <form onSubmit={handleSubmit}>
        <FormGroup controlId="formBasicName">
          <ControlLabel>Product Number</ControlLabel>
          <FormControl type="text"
           placeholder="Table Number"
           value={tableNumber}
           onChange={e => setTableNumber(e.target.value)}
           />
        </FormGroup>

        <FormGroup controlId="formBasicplaces">
          <ControlLabel>Places</ControlLabel>
          <FormControl type="text"
           pattern="[0-9]*"
           placeholder="places"
           value={places}
           onChange={e => setPlaces(e.target.value)}
           />
        </FormGroup>
        <Button variant="primary"
          type="submit"
          disabled={!validateForm()}
          >
          Add
        </Button>
      </form>
    </div>
  );
}
