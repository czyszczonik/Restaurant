import React from 'react'
import {Table} from "react-bootstrap";
import DeleteTable from "../../../services/boss/table/DeleteTable"

export default function RenderTables(tables){

  return(
    <div className="TablesTable">
    <h4>Tables</h4>
    <Table striped bordered hover>
        <thead>
          <tr>
            <th>Number</th>
            <th>Places</th>
          </tr>
        </thead>
        <tbody>
        {
          tables.length > 0 ? (
          tables.map(table => (
            <tr key={table.id}>
              <td>{table.number}</td>
              <td>{table.places}</td>
            </tr>
          ))
        ) : (
          <tr>
            <td colSpan={3}>No tables</td>
          </tr>
        )}
      </tbody>
    </Table>
    </div>
  );
}
