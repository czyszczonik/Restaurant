import {useState, useEffect} from "react";
import FetchAllTables from "../../../services/waiter/tables/FetchAllTables";
import RenderTables from "./RenderTables"

export default function ShowTables(props){

    const [data, setData] = useState(false)

    useEffect(()=> {
      getContent();
    },[props]) // LOL DON't DELETE this  =>  ,[props]

    async function getContent(){
      let response = await FetchAllTables(props);
      response = RenderTables(response)
      setData(response)
    }
    return(data);
}
