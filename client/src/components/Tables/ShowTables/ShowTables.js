import {useState, useEffect} from "react";
import "./ShowTables.css";
import FetchTables from "../../../services/boss/table/FetchTables";
import RenderTables from "./RenderTables"

export default function ShowTables(props, handleUpdate){

const [data, setData] = useState(false)

useEffect(()=> {
  getContent();
},[props]) // LOL DON't DELETE this  =>  ,[props]

async function getContent(){
  let response = await FetchTables(props);
  response = RenderTables(response, props, handleUpdate)
  setData(response)
}
    return(data);
}
