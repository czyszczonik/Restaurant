import React, {useState} from "react";
import { FormGroup, FormControl, ControlLabel, Button } from "react-bootstrap";
import "./EditTable.css";
import UpdateTable from "../../../services/boss/table/UpdateTable";

export default function EditTable(props, table, setEditing) {

  const [tableNumber, setTableNumber] = useState(table.number);
  const [places, setPlaces] = useState(table.places);

  function handleSubmit(event){
    event.preventDefault();
    UpdateTable(table.id, tableNumber, places, props.token);
    setEditing(false);
    props.history.push("/tables")
  }

  function handleCancel(){
    setEditing(false);
    props.history.push("/tables")
  }

  function validateForm(){
    return tableNumber.length > 0 && places >= 0;
  }

  return (
    <div className="EditTable">
    <h4>Update table</h4>
      <form onSubmit={handleSubmit}>
        <FormGroup controlId="formBasicName">
          <ControlLabel>Table Number</ControlLabel>
          <FormControl type="text"
           defaultValue={table.number}
           onChange={e => setTableNumber(e.target.value)}
           />
        </FormGroup>

        <FormGroup controlId="formBasicplaces">
          <ControlLabel>Places</ControlLabel>
          <FormControl type="text"
           pattern="[0-9]*"
           defaultValue={table.places}
           onChange={e => setPlaces(e.target.value)}
           />
        </FormGroup>
        <Button variant="primary"
          type="submit"
          disabled={!validateForm()}
          >
          Update
        </Button>
        <Button variant="primary"
          onClick={()=>handleCancel()}
          >
          Cancel
        </Button>
      </form>
    </div>
  );
}
