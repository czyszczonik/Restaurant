import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "../containers/Home/Home";
import NotFound from "../containers/NotFound/NotFound";
import Login from "../containers/Login/Login";
import Products from "../containers/Products/Products";
import Tables from "../containers/Tables/Tables"
import BossOrders from "../containers/BossOrders/BossOrders"
import AppliedRoute from "./AppliedRoute"
import Orders from "../containers/Orders/Orders"
import OrderItems from "../containers/Orders/OrderItems"
import WaiterProducts from "../containers/Products/WaiterProducts";
import WaiterTables from "../containers/Tables/WaiterTables";
import CookOrders from "../containers/CookOrders/CookOrders";

export default function Routes({ appProps }) {
  return (
    <Switch>
      <AppliedRoute path="/" exact component={Home} appProps={appProps} />
      <AppliedRoute path="/login" exact component={Login} appProps={appProps} />
      <AppliedRoute path="/products" exact component={Products} appProps={appProps} />
      <AppliedRoute path="/tables" exact component={Tables} appProps={appProps} />
      <AppliedRoute path="/bossorders" exact component={BossOrders} appProps={appProps} />

      <AppliedRoute path="/orders" exact component={Orders} appProps={appProps} />
      <AppliedRoute path="/orderItems" exact component={OrderItems} appProps={appProps} />
      <AppliedRoute path="/products/waiter" exact component={WaiterProducts} appProps={appProps} />
      <AppliedRoute path="/tables/waiter" exact component={WaiterTables} appProps={appProps} />
      <AppliedRoute path="/cook/orders" exact component={CookOrders} appProps={appProps}/>

      <Route component={NotFound} />
    </Switch>
  );
}
