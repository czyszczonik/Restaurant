import React, { useState } from "react";
import { Link, withRouter } from "react-router-dom";
import { Nav, Navbar, NavItem } from "react-bootstrap";
import "./App.css";
import Routes from "./components/Routes";
import { LinkContainer } from "react-router-bootstrap";

function App(props) {

const [isAuthenticated, userHasAuthenticated] = useState(false);
const [name, setName] = useState("");
const [token, setToken] = useState("");
const [role, setRole] = useState("");

function handleLogout() {
  userHasAuthenticated(false);
  setName("");
  setRole("");
  setToken("");
  props.history.push("/login");
}

const bossNavbar = <>
  <LinkContainer to="/products">
    <NavItem>Products</NavItem>
  </LinkContainer>
  <LinkContainer to="/tables">
    <NavItem>Tables</NavItem>
  </LinkContainer>
  <LinkContainer to="/bossorders">
    <NavItem>Orders</NavItem>
  </LinkContainer>
  <NavItem onClick={handleLogout}>Logout</NavItem>
  </>;

const waiterNavbar = <>
  <LinkContainer to="/products/waiter">
    <NavItem>Products</NavItem>
  </LinkContainer>
  <LinkContainer to="/tables/waiter">
    <NavItem>Tables</NavItem>
  </LinkContainer>
  <LinkContainer to="/orders">
    <NavItem>Orders</NavItem>
  </LinkContainer>
  <NavItem onClick={handleLogout}>Logout</NavItem>
  </>;

const cookNavbar = <>
  <LinkContainer to="/cook/orders">
    <NavItem>Orders</NavItem>
  </LinkContainer>
  <NavItem onClick={handleLogout}>Logout</NavItem>
  </>;

const defaultNavbar = <NavItem onClick={handleLogout}>Logout</NavItem>

const loginNavbar = <>
  <LinkContainer to="/login">
    <NavItem>Login</NavItem>
  </LinkContainer>
  </>

function takeLinks(isAuthenticated,role){
  if(isAuthenticated) {
    switch(role){
      case("BOSS"):
        return bossNavbar;
      case("WAITER"):
        return waiterNavbar;
      case("COOK"):
        return cookNavbar;
     default:
        return defaultNavbar;
    }
  }
  else {
    return loginNavbar;
  }
}

  return (
    <div className="App container">
      <Navbar fluid collapseOnSelect>
        <Navbar.Header>
          <Navbar.Brand>
          {
            role === "BOSS" ?
           <Link to="/">Welcome my{name ? " "+name : ""}</Link>
         : <Link to="/">Welcome{name ? " "+name : ""}</Link>
          }
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav pullRight>
          {takeLinks(isAuthenticated,role)}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <Routes appProps={{isAuthenticated, userHasAuthenticated, setName, setToken, setRole, name, role, token}} />
    </div>
  );
}



export default withRouter(App);
