export default async function ChangeActivityProduct(id, token){

  return(
    fetch('http://localhost:8080/api/boss/products/change/'+id, {
      method: 'PUT',
      headers: {
        "Authorization": token
      },
      body: JSON.stringify({
        id: id
      })
    }).then(result => console.log(result))
  )
}
