export default async function FetchProducts(props){

  return(
    fetch('http://localhost:8080/api/boss/products/get', {
      method: 'GET',
      headers: {
        "Authorization": props.token      }
    }).then(response => response.json())
  )
}
