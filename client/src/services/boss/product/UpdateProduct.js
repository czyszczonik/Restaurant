export default async function UpdateProduct(id, name, price, active, token){

  return(
    fetch('http://localhost:8080/api/boss/products/update', {
      method: 'PUT',
      headers: {
        "Authorization": token,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        id:id,
        name: name,
        price: price,
        active: active
      })
    })
  )
}
