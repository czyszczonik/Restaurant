export default async function DeleteProduct(id, token){

  return(
    fetch('http://localhost:8080/api/boss/products/delete/'+id, {
      method: 'DELETE',
      headers: {
        "Authorization": token,
      }
    })
  )
}
