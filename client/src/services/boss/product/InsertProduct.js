export default async function InsertProduct(name, price, token){

  return(
    fetch('http://localhost:8080/api/boss/products/new', {
      method: 'POST',
      headers: {
        "Authorization": token,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        name: name,
        price: price
      })
    })
  )
}
