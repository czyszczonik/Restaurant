export default async function InsertTable(number, places, token){

  return(
    fetch('http://localhost:8080/api/boss/tables/new', {
      method: 'POST',
      headers: {
        "Authorization": token,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        number: number,
        places: places
      })
    })
  )
}
