export default async function DeleteTable(id, token){

  return(
    fetch('http://localhost:8080/api/boss/tables/delete/'+id, {
      method: 'DELETE',
      headers: {
        "Authorization": token,
      }
    })
  )
}
