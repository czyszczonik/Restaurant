export default async function UpdateTable(id, number, places, token){

  return(
    fetch('http://localhost:8080/api/boss/tables/update', {
      method: 'PUT',
      headers: {
        "Authorization": token,
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        number: number,
        places: places,
        id:id
      })
    }).then(resp => console.log(resp))
  )
}
