export default async function FetchTables(props){

  return(
    fetch('http://localhost:8080/api/boss/tables/get', {
      method: 'GET',
      headers: {
        "Authorization": props.token      }
    }).then(response => response.json())
  )
}
