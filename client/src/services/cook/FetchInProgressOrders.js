export default async function FetchInProgressOrders(props){

    return(
      fetch('http://localhost:8080/api/cook/orders/progress', {
        method: 'GET',
        headers: {
          "Authorization": props.token
        }
      }).then(response => response.json())
    )
  }
