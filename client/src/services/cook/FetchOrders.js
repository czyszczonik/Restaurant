export default async function FetchOrdered(props){

    return(
      fetch('http://localhost:8080/api/cook/orders/ordered', {
        method: 'GET',
        headers: {
          "Authorization": props.token
        }
      }).then(response => response.json())
    )
  }
