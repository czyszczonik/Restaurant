import jwt_decode from "jwt-decode"

export default async function LoginService(login, password, props){

  function handleOk(response){
    if(!response)
      return false;
      props.userHasAuthenticated(true);
      props.setToken(response);
      props.setName(jwt_decode(response).sub[0].toUpperCase()+jwt_decode(response).sub.slice(1));
      props.setRole(jwt_decode(response).roles[0]);
      return true;
    }

  return(
    fetch('http://localhost:8080/login/', {
      method: 'POST',
      body: JSON.stringify({
        username: login,
        password: password
      })
    }).then(response => (response.ok ? response.text() : false))
    .then(text => handleOk(text))
  )
}
