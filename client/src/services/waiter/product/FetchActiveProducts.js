export default function FetchActiveProducts(props){

    return(
      fetch('http://localhost:8080/api/waiter/products/get', {
        method: 'GET',
        headers: {
          "Authorization": props.token
        }
      }).then(response => response.json())
    )
}
