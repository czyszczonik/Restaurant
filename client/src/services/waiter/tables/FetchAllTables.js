export default function FetchProducts(props){

    return(
      fetch('http://localhost:8080/api/waiter/tables/get', {
        method: 'GET',
        headers: {
          "Authorization": props.token      
        }
      }).then(response => response.json())
    )
}
  