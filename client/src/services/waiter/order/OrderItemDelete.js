export default async function OrderItemDelete(props, orderItemId){

    return(
      fetch('http://localhost:8080/api/waiter/orderItem/delete/' + orderItemId, {
        method: 'GET',
        headers: {
          "Authorization": props.token      
        },
      })
    )
}