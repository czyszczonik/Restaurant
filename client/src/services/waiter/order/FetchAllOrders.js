export default async function FetchAllOrders(props){

    return(
      fetch('http://localhost:8080/api/waiter/orders/getActive', {
        method: 'GET',
        headers: {
          "Authorization": props.token
        }
      }).then(response => response.json())
    )
  }
