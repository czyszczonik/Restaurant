export default async function UpdateOrderStatus(props, orderID, status) {

    return(
      fetch('http://localhost:8080/api/waiter/orders/status/update?'
        + "id=" + orderID + "&"
        + "payed=" + status, {
        method: 'GET',
        headers: {
          "Authorization": props.token      
        }
      })
    )
}