export default function CreateNewOrder(props, table){

    return(
      fetch('http://localhost:8080/api/waiter/orders/new?tableNumber=' 
        + table, {
          method: 'GET',
          headers: {
            "Authorization": props.token      
          },
        
      }).then(response => response.json())
    )
}