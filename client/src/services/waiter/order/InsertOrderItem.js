export default async function InsertNewOrderItem(props, orderID, orderProductName, orderQuantity){

    return(
      fetch('http://localhost:8080/api/waiter/orderItems/new?'
            + "orderId=" + orderID + "&"
            + "productName=" + orderProductName + "&"
            + "quantity=" + orderQuantity, {
        method: 'GET',
        headers: {
          "Authorization": props.token      
        }
      })
    )
}