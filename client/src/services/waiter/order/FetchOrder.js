export default async function FetchOrder(props, orderID){

    return(
      fetch('http://localhost:8080/api/waiter/order/' + orderID, {
        method: 'GET',
        headers: {
          "Authorization": props.token      
        },
      }).then(response => response.json())
    )
}