import React, {useState, useEffect} from "react";
import {Table} from "react-bootstrap"
import FetchInProgressOrders from "../../services/cook/FetchInProgressOrders";
import FetchOrders from "../../services/cook/FetchOrders";
import RenderInProgressOrders from "./RenderInProgressOrders"
import RenderOrderedOrders from "./RenderOrderedOrders"

export default function CookOrders(props){

    const [ordered, setOrdered] = useState(false)
    const [inProgress, setInProgress] = useState(false)

    useEffect(()=> {
      getContent();
    },[props]) // LOL DON't DELETE this  =>  ,[props]

    async function getContent(){
      let orderedResponse = await FetchOrders(props);
      orderedResponse = RenderOrderedOrders(orderedResponse, props)
      setOrdered(orderedResponse)
      let inProgressResponse = await FetchInProgressOrders(props);
      inProgressResponse = RenderInProgressOrders(inProgressResponse, props)
      setInProgress(inProgressResponse)
    }

    return(<div>
      <div>
        <h4>In Progress</h4>
        <Table striped bordered hover>
            <thead>
              <tr>
                <th>Table</th>
                <th>Name</th>
                <th>Quantity</th>
                <th>Update</th>
              </tr>
            </thead>
            <tbody>
                      <tr>
                        <td colSpan={4}>No Orders</td>
                      </tr>

            </tbody>
        </Table>
        </div>
        <div>
        <h4>Orders</h4>
        <Table striped bordered hover>
            <thead>
              <tr>
                <th>Table</th>
                <th>Name</th>
                <th>Quantity</th>
                <th>Update</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td colSpan={4}>No Orders</td>
              </tr>
            </tbody>
        </Table>
        </div>
    </div>
  );
}
