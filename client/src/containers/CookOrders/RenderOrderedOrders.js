import React from "react";
import {Table} from "react-bootstrap";

export default function RenderOrderedOrders(orders, props){


  return(
      <div>
          <h4>Orders</h4>
          <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Table</th>
                  <th>Name</th>
                  <th>Quantity</th>
                  <th>Update</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td colSpan={4}>No Orders</td>
                </tr>
              </tbody>
          </Table>
      </div>
  );
}
