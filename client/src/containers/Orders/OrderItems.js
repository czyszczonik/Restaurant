import React, {useState} from "react";
import "./Orders.css";
import OrderItemsProducts from '../../components/Orders/OrderItems/OrderItemsProducts'
import NewItemsProduct from "../../components/Orders/OrderItems/NewItemsProducts"
import ProductsList from "../../components/Orders/OrderItems/ProductsList"


export default function OrdersItems(props) {

    const[order, setOrderId] = useState(props.location.state.order);

    return (
      <div className="OrderItems">
        {OrderItemsProducts(props, order)}
        {NewItemsProduct(props, order)}
        {ProductsList(props)}
      </div>
    );
}
