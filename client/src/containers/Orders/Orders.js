import React,  {useState} from "react";
import "./Orders.css";
import OrdersTab from "../../components/Orders/OrdersTab.js"
import AddOrder from "../../components/Orders/AddOrder/AddOrder";

export default function Orders(props) {

  // const[order, setOrder] = useState("")

  function handleUpdate(order){
      props.history.push({
        pathname: "/orderItems",
        state: { order: order}
      })
  }

   return (
     <div className="Orders">
        {AddOrder(props)}
        {OrdersTab(props, handleUpdate)}
     </div>
   );
}
