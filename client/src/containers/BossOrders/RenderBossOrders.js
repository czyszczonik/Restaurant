import React from "react";
import {Table} from "react-bootstrap";
import "./BossOrders.css";

export default function RenderBossOrders(orders, props){

  function getTime(date) {
    // var datestring = date.getDate()  + "-" + (date.getMonth()+1) + "-" + date.getFullYear() + " " + date.getHours() + ":" + date.getMinutes();
    var datestring = date.substring(0, 10) + " " + date.substring(11, 16)
    return datestring
  }

  return(
      <div>
          <h4>Orders</h4>
          <Table striped bordered hover>
              <thead>
                <tr>
                  <th>Table</th>
                  <th>Date</th>
                  <th>Items</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
                  {
                    orders.length > 0
                      ? (orders.map(order => (
                        <tr key={order.id} rowSpan="3">
                          <td>{order.table.number}</td>
                          <td>{getTime(order.date)}</td>
                          {/* <td>{order.date}</td> */}
                          <td>
                            <Table striped hover bordered>
                              <tbody>{
                                order.orderItems.map( item =>
                                    <tr key={item.id}>
                                      <td>{item.quantity}</td>
                                      <td>{item.product.name}</td>
                                      <td>{item.price}</td>
                                    </tr>
                                  )
                                }
                              </tbody>
                            </Table>
                          </td>
                          <td>{order.totalPrice}</td>

                        </tr>
                        )))
                      : (
                        <tr>
                          <td colSpan={4}>No Orders</td>
                        </tr>
                        )
                  }
              </tbody>
          </Table>
      </div>
  );
}
