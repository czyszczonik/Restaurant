import {useState, useEffect} from "react";
import FetchAllOrders from "../../services/boss/orders/FetchAllOrders";
import RenderBossOrders from "./RenderBossOrders"

export default function BossOrders(props){

    const [data, setData] = useState(false)

    useEffect(()=> {
      getContent();
    },[props]) // LOL DON't DELETE this  =>  ,[props]

    async function getContent(){
      let response = await FetchAllOrders(props);
      response = RenderBossOrders(response, props)
      setData(response)
    }

    return(data);
}
