import React, {useState} from "react";
import ShowTables from "../../components/Tables/Waiter/ShowTable"

export default function WaiterTables(props) {

  return (
    <div className="Tables">
      {ShowTables(props)}
    </div>
  );
}
