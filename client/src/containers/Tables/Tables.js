import React, {useState} from "react";
import "./Tables.css";
import AddTable from "../../components/Tables/AddTable/AddTable"
import ShowTables from "../../components/Tables/ShowTables/ShowTables"
import EditTable from "../../components/Tables/EditTable/EditTable"

export default function Tables(props) {

const[editing, setEditing] = useState(false);
const[table, setTable] = useState("");

function handleUpdate(table){
    setTable(table);
    setEditing(true);
}

  return (
    <div className="Tables">
      {editing ? EditTable(props, table, setEditing)
        : AddTable(props)}
      {ShowTables(props, handleUpdate)}
    </div>
  );
}
