import React, {useState} from "react";
import "./Products.css";
import AddProduct from "../../components/Products/AddProduct/AddProduct"
import ProductsTable from "../../components/Products/ProductsTable/ProductsTable"
import EditProduct from "../../components/Products/EditProduct/EditProduct"

export default function Products(props) {

const[editing, setEditing] = useState(false);
const[product, setProduct] = useState("");

function handleUpdate(product){
    setProduct(product);
    setEditing(true);
}

  return (
    <div className="Products">
      {editing ? EditProduct(props, product, setEditing)
        : AddProduct(props)}
      {ProductsTable(props, handleUpdate)}
    </div>
  );
}
