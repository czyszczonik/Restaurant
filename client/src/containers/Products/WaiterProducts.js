import React from "react";
import ProductsList from "../../components/Orders/OrderItems/ProductsList"

export default function WaiterProducts(props) {

    return (
        <div className="WaiterProducts">
            {ProductsList(props)}
        </div>
    )
}
