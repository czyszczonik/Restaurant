import React from "react";
import "./NotFound.css";

export default function NotFound(){
  return(
    <div className="NotFound">
      <div className="NFText">
        <h4 class="green">Sorry</h4>
        <div className="white">404</div>
        <h4 className="red">pasta not found!</h4>
      </div>
    </div>
  );
}
