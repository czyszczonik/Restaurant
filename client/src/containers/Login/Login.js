import React, { useState } from "react";
import { FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import "./Login.css";
import LoginService from "../../services/LoginService";
import LoaderButton from "../../components/LoaderButton/LoaderButton";

export default function Login(props) {

  const [Username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  function validateForm() {
    return Username.length > 0 && password.length > 0;
  }

  async function handleSubmit(event) {
    event.preventDefault();
    setIsLoading(true);
    var logged = await LoginService(Username, password, props);
    logged ? props.history.push("/") : wrongCredentialas();
  }

  function wrongCredentialas(){
      setIsLoading(false);
      alert("Wrong login or password")
  }

  return (
    <div className="Login">
      <form onSubmit={handleSubmit}>
        <FormGroup controlId="User" bsSize="large">
          <ControlLabel>Username</ControlLabel>
          <FormControl
            autoFocus
            type="Username"
            value={Username}
            onChange={e => setUsername(e.target.value)}
          />
        </FormGroup>
        <FormGroup controlId="password" bsSize="large">
          <ControlLabel>Password</ControlLabel>
          <FormControl
            value={password}
            onChange={e => setPassword(e.target.value)}
            type="password"
          />
        </FormGroup>
        <LoaderButton
            block type="submit"
            bsSize="large"
            isLoading={isLoading}
            disabled={!validateForm()}
        >
  Login
</LoaderButton>

      </form>
    </div>
  );
}
