package org.studies.restaurant.security.service

import org.studies.restaurant.security.model.User
import org.studies.restaurant.security.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service

@Service
class SecurityUserDetailsService @Autowired
constructor(private val userRepository: UserRepository) : UserDetailsService {
    override fun loadUserByUsername(name: String): UserDetails {
        val user: User = userRepository.findByUsername(name) ?: throw(UsernameNotFoundException("Username not found"))
        return SecurityUserDetails(user)
    }
}