package org.studies.restaurant.security.repository

import org.studies.restaurant.security.model.User
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.MongoRepository
import org.springframework.data.rest.core.annotation.RepositoryRestResource

@RepositoryRestResource
interface UserRepository : MongoRepository<User, ObjectId> {
    fun findByUsername(username: String): User?
}
