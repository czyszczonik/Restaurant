package org.studies.restaurant.security.jwt

import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.apache.logging.log4j.LogManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.studies.restaurant.security.model.User
import java.util.*
import java.util.concurrent.TimeUnit
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import kotlin.collections.HashMap

internal object JWTUtils {
    private const val expiration: Long = 1000L
    private const val secret = "secret"
    private const val header = "Authorization"

    private val logger = LogManager.getLogger(JWTUtils::class.java)

    private fun User.createJwt(): String {
        val claims = HashMap<String, Any>()
        claims["roles"] = this.roles
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(this.username)
                .setExpiration(Date(Date().time + TimeUnit.HOURS.toMillis(expiration)))
                .signWith(SignatureAlgorithm.HS512, secret).compact()
    }

    fun addAuthentication(response: HttpServletResponse, user: User) {
        val jwt = user.createJwt()
        response.setHeader("Access-Control-Allow-Origin", "http://localhost:3000")
        response.writer.write(jwt)
        response.writer.flush()
        response.writer.close()
    }

    fun getAuthentication(request: HttpServletRequest): Authentication? {
        val token = request.getHeader(header) ?: return null
        val tokenBody = Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .body

        val username: String = tokenBody.subject
        @Suppress("UNCHECKED_CAST")
        val roles = tokenBody["roles"] as List<String>
        val res = roles.mapTo(LinkedList<GrantedAuthority>()) { SimpleGrantedAuthority(it) }
        logger.info("$username logged in with authorities $res")
        return UsernamePasswordAuthenticationToken(username, null, res)
    }
}