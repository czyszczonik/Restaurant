package org.studies.restaurant.security.jwt

data class AccountCredentials(val username: String = "", val password: String = "")