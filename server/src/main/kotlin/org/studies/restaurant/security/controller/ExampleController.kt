package org.studies.restaurant.security.controller

import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import java.security.Principal

@RestController
class ExampleController {

    @GetMapping("example/boss/")
    @PreAuthorize("hasAuthority('BOSS')")
    fun boss(principal: Principal?) = principal?.name ?: "You are not logged in"

    @GetMapping("example/waiter/")
    @PreAuthorize("hasAuthority('BOSS') or hasAuthority('WAITER')")
    fun waiter(principal: Principal?) = principal?.name ?: "You are not logged in"


    @GetMapping("example/cook/")
    @PreAuthorize("hasAuthority('BOSS') or hasAuthority('COOK')")
    fun cook(principal: Principal?) = principal?.name ?: "You are not logged in"

}
