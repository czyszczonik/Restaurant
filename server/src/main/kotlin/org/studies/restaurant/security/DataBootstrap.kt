package org.studies.restaurant.security

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.stereotype.Component
import org.studies.restaurant.security.model.User
import org.studies.restaurant.security.repository.UserRepository

@Component
class DataBootstrap @Autowired
constructor(private val userRepository: UserRepository) : CommandLineRunner {
    override fun run(vararg args: String) {
        //{noop} new spring password storage
        userRepository.deleteAll()
        val boss = User(username = "boss", password = "{noop}boss", roles = mutableListOf("BOSS"))
        userRepository.save(boss)
        val waiter = User(username = "waiter", password = "{noop}waiter", roles = mutableListOf("WAITER"))
        userRepository.save(waiter)
        val cook = User(username = "cook", password = "{noop}cook", roles = mutableListOf("COOK"))
        userRepository.save(cook)
    }
}