package org.studies.restaurant.security.config

import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import org.springframework.security.web.session.SessionManagementFilter
import org.studies.restaurant.security.jwt.JWTAuthenticationFilter
import org.studies.restaurant.security.jwt.JWTLoginFilter


@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled=true, prePostEnabled=true)
class SecurityConfig : WebSecurityConfigurerAdapter() {
    override fun configure(http: HttpSecurity) {
        http.
                csrf()
                    .disable()
                    .antMatcher("/**").authorizeRequests()
                    .antMatchers("/login/").permitAll()
                    .anyRequest().authenticated()
                .and()
                .addFilterBefore(CORSFilter(), SessionManagementFilter::class.java)
                    .addFilterBefore(JWTLoginFilter("/login/", authenticationManager()),
                        UsernamePasswordAuthenticationFilter::class.java)
                    .addFilterBefore(JWTAuthenticationFilter(), UsernamePasswordAuthenticationFilter::class.java)
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
    }

}
