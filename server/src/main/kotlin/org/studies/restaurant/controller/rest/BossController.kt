package org.studies.restaurant.controller.rest

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import org.studies.restaurant.models.*
import org.studies.restaurant.repository.utils.OrderItemManager
import org.studies.restaurant.repository.utils.OrderManager
import org.studies.restaurant.repository.utils.ProductManager
import org.studies.restaurant.repository.utils.TableManager

@RestController
@PreAuthorize("hasAuthority('BOSS')")
@RequestMapping("/api/boss")
class BossController {

    @Autowired
    lateinit var tableManager: TableManager

    @Autowired
    lateinit var productManager: ProductManager

    @Autowired
    lateinit var orderManager: OrderManager

    @Autowired
    lateinit var orderItemManager: OrderItemManager


    @GetMapping("/tables/get")
    fun getTables(): List<Table> {
        return tableManager.findAllTables()
    }

    @DeleteMapping("/tables/delete/{number}")
    fun deleteTable(@PathVariable number: String) {
        tableManager.delete(number)
    }

    @PostMapping("/tables/new")
    fun newTable(@RequestBody table: Table) {
        tableManager.insert(table.number, table.places)
    }

    @PutMapping("/tables/update")
    fun updateTable(@RequestBody table: Table) {
        return tableManager.update(table)
    }

    @PostMapping("/orders/new")
    fun createNewOrder(@RequestParam tableNumber: String): Order {
        return orderManager.insert(tableNumber)
    }

    @PostMapping("/orderItems/new")
    fun createNewOrderItem(@RequestParam productName: String,
                           @RequestParam orderId: String,
                           @RequestParam quantity: Int): OrderItem {

        return orderItemManager.insert(productName, orderId, quantity)
    }

    @PutMapping("/orders/status/update")
    fun updateOrderStatus(@RequestParam id: String, @RequestParam payed: Boolean) {
        return orderManager.updateOrder(id, payed)
    }

    @PutMapping("/orderItems/update")
    fun updateOrderItem(@RequestParam id: String,
                        @RequestParam productId: String,
                        @RequestParam quantity: Int,
                        @RequestParam status: OrderItemStatus) {
        return orderItemManager.updateOrderItem(id, productId, quantity, status)
    }

    @GetMapping("/orders/get")
    fun getOrders(): List<Order> {
        return orderManager.findAllOrders()
    }

    @GetMapping("/orderItems/get")
    fun getOrderItems(): List<OrderItem> {
        return orderItemManager.findAll()
    }

    @GetMapping("/products/get")
    fun getProducts(): List<Product> {
        return productManager.findAll()
    }

    @PostMapping("/products/new")
    fun newProduct(@RequestBody product: Product) {
        productManager.insert(product.name, product.price)
    }

    @PutMapping("/products/update")
    fun updateProduct(@RequestBody product: Product) {
        productManager.updateProduct(product.id, product.name, product.price, product.active)
    }

    @PutMapping("products/change/{id}")
    fun changeProductActivity(@PathVariable id: String){
        productManager.changeActivity(id)
    }

    @DeleteMapping("/products/delete/{id}")
    fun deleteProduct(@PathVariable id: String) {
        productManager.deleteById(id)
    }
}