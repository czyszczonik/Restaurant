package org.studies.restaurant.controller.rest

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import org.studies.restaurant.repository.utils.OrderManager
import org.studies.restaurant.repository.utils.OrderItemManager
import org.studies.restaurant.repository.utils.ProductManager
import org.studies.restaurant.models.OrderItemStatus


@RestController
@PreAuthorize("hasAuthority('COOK')")
@RequestMapping("/api/cook")
class CookController {

    @Autowired
    lateinit var productManager: ProductManager

    @Autowired
    lateinit var orderManager: OrderManager

    @Autowired
    lateinit var orderItemManager: OrderItemManager

    @GetMapping("/orders/ordered")
    fun getOrderItems(){
        orderItemManager.findOrdersByStatus(1)
    }

    @GetMapping("/orders/progress")
    fun getInProgressItems(){
        orderItemManager.findOrdersByStatus(2)
    }


    @PutMapping("/orderItems/status/update")
    fun updateOrderItem(@RequestParam id: String,
                        @RequestParam productId: String,
                        @RequestParam quantity: Int,
                        @RequestParam status: OrderItemStatus) {

        return orderItemManager.updateOrderItem(id, productId, quantity, status)
    }


}