package org.studies.restaurant.controller.rest

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*
import org.studies.restaurant.models.*
import org.studies.restaurant.repository.utils.OrderManager
import org.studies.restaurant.repository.utils.OrderItemManager
import org.studies.restaurant.repository.utils.ProductManager
import org.studies.restaurant.repository.utils.TableManager


@RestController
@PreAuthorize("hasAuthority('WAITER')")
@RequestMapping("/api/waiter")
class WaiterController {

    @Autowired
    lateinit var tableManager: TableManager

    @Autowired
    lateinit var productManager: ProductManager

    @Autowired
    lateinit var orderManager: OrderManager

    @Autowired
    lateinit var orderItemManager: OrderItemManager


    @GetMapping("/tables/get")
    fun getTables(): List<Table> {
        return tableManager.findAllTables()
    }


    @GetMapping("/orders/new")
    fun createNewOrder(@RequestParam tableNumber: String): Order {
        return orderManager.insert(tableNumber)
    }

    @GetMapping("/orderItems/new")
    fun createNewOrderItem(@RequestParam productName: String,
                           @RequestParam orderId: String,
                           @RequestParam quantity: Int): OrderItem {

        return orderItemManager.insert(productName, orderId, quantity)
    }

    @GetMapping("/orders/status/update")
    fun updateOrderStatus(@RequestParam id: String, @RequestParam payed: Boolean) {
        return orderManager.updateOrder(id, payed)
    }

    @GetMapping("/orderItems/update")
    fun updateOrderItem(@RequestParam id: String,
                        @RequestParam productId: String,
                        @RequestParam quantity: Int,
                        @RequestParam status: OrderItemStatus) {
        return orderItemManager.updateOrderItem(id, productId, quantity, status)
    }

    @GetMapping("/order/delete/{id}")
    fun deleteOrder(@PathVariable id: String) {
        return orderManager.deleteById(id)
    }

    @GetMapping("/orderItem/delete/{id}")
    fun deleteOrderItem(@PathVariable id: String) {
        return orderItemManager.delete(id)
    }

    @GetMapping("/orders/getActive")
    fun getActiveOrders(): List<Order> {
        return orderManager.getActiveOrders()
    }

    @GetMapping("/orders/get")
    fun getAllOrders(): List<Order> {
        return orderManager.findAllOrders()
    }

    @GetMapping("/products/get")
    fun getAllProducts(): List<Product> {
        return productManager.findAllActive()
    }

    @GetMapping("/order/{id}")
    fun getOrder(@PathVariable id: String): Order {
        return orderManager.findOrderById(id)
    }

    @GetMapping("/delete/all")
    fun deleteAll() {
        orderManager.deleteAll()
        orderItemManager.deleteAll()
    }
}