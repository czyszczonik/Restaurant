package org.studies.restaurant.models

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class OrderItem(
    var quantity: Int,
    var product: Product,
    var orderId: String,
    var price: Float
) {
    var status = OrderItemStatus.ORDERED
    @Id
    lateinit var id: String
}