package org.studies.restaurant.models

enum class OrderItemStatus {
    ORDERED, IN_PROGRESS, READY, CANCELLED
}