package org.studies.restaurant.models

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class Product(
        var name: String,
        var price: Float
) {
    @Id
    lateinit var id: String
    var active: Boolean = true
}