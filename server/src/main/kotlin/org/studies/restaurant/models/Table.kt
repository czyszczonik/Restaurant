package org.studies.restaurant.models

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class Table(var number: String) {
    // I know how it looks like xD
    // Table number is string in  case we want to name tables like `2E` or whatever
    var places: Int = 0
    @Id
    lateinit var id: String
}