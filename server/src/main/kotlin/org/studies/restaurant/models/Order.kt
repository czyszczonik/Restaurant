package org.studies.restaurant.models

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.DBRef
import org.springframework.data.mongodb.core.mapping.Document
import java.util.*

@Document
data class Order(
        var date: Date
) {
    var payed: Boolean = false
    @DBRef
    lateinit var table: Table
    @Id
    lateinit var id: String

    var orderItems = mutableListOf<OrderItem>()
    var totalPrice: Float = 0.0f
}