package org.studies.restaurant.repository.utils

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestParam
import org.studies.restaurant.models.Table
import org.studies.restaurant.repository.TableRepository

@Service
class TableManager {

    @Autowired
    lateinit var repository: TableRepository

    fun findAllTables() = repository.findAll()

    fun insert(number: String, places: Int) {
        val table = Table(number)
        table.places = places
        repository.insert(table)
    }

    fun delete(number: String) {
        repository.deleteById(number)
    }

    fun update(table: Table){
        val new = repository.findById(table.id)
        new.number = table.number
        new.places = table.places
        repository.save(new)
    }

    fun getTable(table: String) = repository.getTableByNumber(table)

}