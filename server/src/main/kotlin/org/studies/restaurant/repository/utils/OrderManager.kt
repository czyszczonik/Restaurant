package org.studies.restaurant.repository.utils

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.*
import org.studies.restaurant.models.Order
import org.studies.restaurant.repository.OrderRepository
import org.studies.restaurant.repository.TableRepository
import java.util.*

@Service
class OrderManager {

    @Autowired
    lateinit var tableRepository: TableRepository

    @Autowired
    lateinit var orderRepository: OrderRepository

    fun findAllOrders() = orderRepository.findAll()


    fun insert(tableNumber: String): Order {
        val order =  Order(Date())
        val table = tableRepository.getTableByNumber(tableNumber)
        //TODO fallback if table does not exist
        order.table = table?.get(0)
        return orderRepository.insert(order)
    }

    fun deleteById(id: String) {
        orderRepository.deleteById(id)
    }

    fun deleteAllOrders() {
        orderRepository.deleteAll()
    }

    fun updateOrder(id: String, payed: Boolean) {
        val order = orderRepository.findOrderById(id)
        order.payed = payed
        orderRepository.save(order)
    }

    fun getActiveOrders(): List<Order> {
        return orderRepository.findByPayedIsFalse()
    }

    fun findOrderById(id: String): Order {
        return orderRepository.findOrderById(id)
    }

    fun updateOrder(order: Order): Order {
        var totalPrice: Float = 0f
        for(item in order.orderItems) {
            totalPrice += item.price
        }
        order.totalPrice = totalPrice
        return orderRepository.save(order)
    }

    fun deleteAll() {
        return orderRepository.deleteAll()
    }

}