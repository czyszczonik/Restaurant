package org.studies.restaurant.repository.utils

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.studies.restaurant.models.Product
import org.studies.restaurant.repository.ProductRepository

@Service
class ProductManager {

    @Autowired
    lateinit var productRepository: ProductRepository

    fun findAll() = productRepository.findAll().sortedBy { !it.active }

    // Change because it's not optimal :(
    fun findAllActive() = productRepository.findAll().filter { it.active }

    fun insert(name: String, price: Float) {
        val product = Product(name, price)
        productRepository.insert(product)
    }

    fun deleteById(id: String) {
        productRepository.deleteById(id)
    }

    fun updateProduct(id: String,
                      name: String,
                      price: Float,
                      active: Boolean) {

        val product = productRepository.findById(id)
        product.name = name
        product.active = active
        product.price = price
        productRepository.save(product)
    }

    fun changeActivity(id: String) {
        val product = productRepository.findById(id)
        product.active = !product.active
        productRepository.save(product)
    }
}