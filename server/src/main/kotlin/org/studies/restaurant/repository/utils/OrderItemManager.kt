package org.studies.restaurant.repository.utils

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.web.bind.annotation.*
import org.studies.restaurant.models.OrderItem
import org.studies.restaurant.models.OrderItemStatus
import org.studies.restaurant.repository.OrderItemRepository
import org.studies.restaurant.repository.OrderRepository
import org.studies.restaurant.repository.ProductRepository


@Service
class OrderItemManager {

    @Autowired
    lateinit var orderManager: OrderManager

    @Autowired
    lateinit var orderItemRepository: OrderItemRepository

    @Autowired
    lateinit var productRepository: ProductRepository

    fun findAll() = orderItemRepository.findAll()

    fun findOrdersByStatus(index: Int): List<OrderItem> {
        var status = OrderItemStatus.ORDERED
        when (index) {
            1 -> status = OrderItemStatus.ORDERED
            2 -> status = OrderItemStatus.IN_PROGRESS
        }
        return orderItemRepository.findOrderItemsByStatus(status)
    }

    fun insert(productName: String,
               orderId: String,
               quantity: Int): OrderItem {

        //TODO fallback logic if product & order don't exist
        //TODO add new orderItem to Order list
        val product = productRepository.findProductByName(productName)[0]
        val order = orderManager.findOrderById(orderId)

        val orderItemPrice = product.price * quantity
        val orderItem = OrderItem(quantity, product, orderId, orderItemPrice)

        val newOrderItem = orderItemRepository.insert(orderItem)
        order.orderItems.add(orderItem)
        orderManager.updateOrder(order)
        return newOrderItem
    }

    fun delete(id: String) {
        val orderItem = orderItemRepository.findById(id)
        val order = orderManager.findOrderById(orderItem.orderId)
        order.orderItems.remove(orderItem)
        orderManager.updateOrder(order)
        return orderItemRepository.deleteById(id)
    }

    fun updateOrderItem(id: String,
                        productId: String,
                        quantity: Int,
                        status: OrderItemStatus) {

        val orderItem = orderItemRepository.findById(id)
        val product = productRepository.findById(productId)
        val order = orderManager.findOrderById(orderItem.orderId)
        order.orderItems.remove(orderItem)
        orderItem.product = product
        orderItem.quantity = quantity
        orderItem.status = status
        orderItem.price = quantity * product.price

        orderItemRepository.save(orderItem)
        orderManager.updateOrder(order)
    }

    fun deleteAll() {
        orderItemRepository.deleteAll()
    }
}
