package org.studies.restaurant.repository


import org.springframework.data.mongodb.repository.MongoRepository
import org.studies.restaurant.models.*

interface TableRepository: MongoRepository<Table, Long> {
    override fun findAll(): List<Table>
    fun getTableByNumber(table: String): List<Table>
    fun findById(id: String): Table
    fun deleteById(id: String)

}

interface OrderRepository: MongoRepository<Order, Long> {
    override fun findAll(): List<Order>
    fun deleteById(id: String)
    fun findOrderById(id: String): Order
    fun findByPayedIsFalse(): List<Order>
}

interface OrderItemRepository: MongoRepository<OrderItem, Long> {
    override fun findAll(): List<OrderItem>
    fun deleteById(id: String)
    fun findById(id: String): OrderItem
    fun insert(entity: OrderItem): OrderItem
    fun findOrderItemsByStatus(status: OrderItemStatus): List<OrderItem>
}

interface ProductRepository: MongoRepository<Product, Long> {
    override fun findAll(): List<Product>
    fun findProductByName(name: String): List<Product>
    fun findById(id: String): Product
    fun deleteById(id: String)
}