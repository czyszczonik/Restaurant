package org.studies.restaurant.controller.rest

import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.studies.restaurant.repository.utils.OrderItemManager
import org.studies.restaurant.repository.utils.OrderManager
import org.studies.restaurant.repository.utils.ProductManager
import spock.lang.Specification

class CookControllerTest extends Specification {

    MockMvc mockMvc
    OrderItemManager mockOrderItemManager
    OrderManager mockOrderManager
    ProductManager mockProductManager

    def setup() {
        mockOrderItemManager = Mock(OrderItemManager)
        mockProductManager = Mock(ProductManager)
        mockOrderManager = Mock(OrderManager)
        mockMvc = MockMvcBuilders.standaloneSetup(new CookController(orderItemManager: mockOrderItemManager, productManager: mockProductManager, orderManager: mockOrderManager)).build()
    }

    def "load via web service and get a response"() {
        when:
        "Calling web service and get a response"

        def response = mockMvc
                .perform(MockMvcRequestBuilders.get("/api/cook/orderItems/status/update")
                        .contentType(MediaType.TEXT_PLAIN)
                        .param("id", "123")
                        .param("productId", "123")
                        .param("quantity", "1")
                        .param("status", "READY"))
                .andReturn()
                .response

        then: "expect that a valid response occurs when all required parameters are filled"
        response.getStatus() == 405
    }

    def "wrong endpoint should return error 404 response"() {
        when: "Calling a wrong endpoint"

        def call = mockMvc
                .perform(MockMvcRequestBuilders.get("/api/cook/random")
                        .contentType(MediaType.TEXT_PLAIN))
                .andReturn()

        def response = call.response

        then: "expect a wrong response"
        response.getStatus() == 404
    }


    def "malformed request should return error 400"() {
        when:
        "Calling web service and get a response"

        def response = mockMvc
                .perform(MockMvcRequestBuilders.get("/api/cook/orderItems/status/update")
                        .contentType(MediaType.TEXT_PLAIN))
                .andReturn()
                .response

        then: "expect client error response"
        response.getStatus() == 405
    }

}
