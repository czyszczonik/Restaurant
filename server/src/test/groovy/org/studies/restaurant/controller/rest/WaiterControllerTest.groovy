package org.studies.restaurant.controller.rest

import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.studies.restaurant.repository.utils.OrderItemManager
import org.studies.restaurant.repository.utils.OrderManager
import org.studies.restaurant.repository.utils.ProductManager
import org.studies.restaurant.repository.utils.TableManager
import spock.lang.Specification

class WaiterControllerTest extends Specification {

    MockMvc mockMvc
    OrderManager mockOrderManager
    OrderItemManager mockOrderItemManager
    ProductManager mockProductManager
    TableManager mockTableManager

    def setup() {
        mockOrderManager = Mock(OrderManager)
        mockOrderItemManager = Mock(OrderItemManager)
        mockProductManager = Mock(ProductManager)
        mockTableManager = Mock(TableManager)
        mockMvc = MockMvcBuilders.standaloneSetup(new WaiterController(orderManager: mockOrderManager, orderItemManager: mockOrderItemManager, productManager: mockProductManager, tableManager: mockTableManager)).build()
    }

    def "load via web service and get a response"() {
        when:
        "Calling web service and get a response"

        def response = mockMvc
                .perform(MockMvcRequestBuilders.get("/api/waiter/tables/get")
                        .contentType(MediaType.TEXT_PLAIN))
                .andReturn()
                .response

        then: "expect that a valid response occurs"
        response.getStatus() == 200
    }

    def "wrong endpoint should return error 404 response"() {
        when: "Calling a wrong endpoint"

        def call = mockMvc
                .perform(MockMvcRequestBuilders.get("/api/waiter/random")
                        .contentType(MediaType.TEXT_PLAIN))
                .andReturn()

        def response = call.response

        then: "expect a wrong response"
        response.getStatus() == 404
    }


    def "call web service with all required parameters should return OK"() {
        when:
        "Calling web service and get a response"

        def response = mockMvc
                .perform(MockMvcRequestBuilders.get("/api/waiter/orderItems/update")
                        .contentType(MediaType.TEXT_PLAIN)
                        .param("id", "123")
                        .param("productId", "123")
                        .param("quantity", "1")
                        .param("status", "READY"))
                .andReturn()
                .response

        then: "expect that a valid response occurs when all required parameters are filled"
        response.getStatus() == 200
    }

}
